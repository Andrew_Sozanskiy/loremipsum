import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { RouterModule, Routes } from '@angular/router';
import { OrdersComponent } from '@pages/orders/orders.component';
import { GridModule } from '@progress/kendo-angular-grid';
import { SharedModule } from '@shared/shared.module';

const routerConfig: Routes = [
  {
    path: '',
    component: OrdersComponent
  }
];

const MODULES = [
  CommonModule,
  RouterModule.forChild(routerConfig),
  GridModule,
  SharedModule,
  MatSnackBarModule
];

const COMPONENTS = [OrdersComponent];

@NgModule({
  imports: [...MODULES],
  declarations: [...COMPONENTS]
})
export class OrdersModule {}
