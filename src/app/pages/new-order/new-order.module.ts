import { Ng2TelInputModule } from 'ng2-tel-input';

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { RouterModule, Routes } from '@angular/router';
import { NewOrderComponent } from '@pages/new-order/new-order.component';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { GridModule } from '@progress/kendo-angular-grid';
import { IntlModule } from '@progress/kendo-angular-intl';
import { SharedModule } from '@shared/shared.module';

const routerConfig: Routes = [
  {
    path: '',
    component: NewOrderComponent
  }
];

const MODULES = [
  RouterModule.forChild(routerConfig),
  GridModule,
  DateInputsModule,
  IntlModule,
  ReactiveFormsModule,
  FormsModule,
  CommonModule,
  Ng2TelInputModule,
  MatSnackBarModule,
  SharedModule
];

const COMPONENTS = [NewOrderComponent];

@NgModule({
  imports: [...MODULES],
  declarations: [...COMPONENTS]
})
export class NewOrderModule {}
