import * as moment from 'moment';

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Store } from '@ngrx/store';
import { SuccessSnackComponent } from '@shared/components/success-snack/success-snack';
import { EmailValidator } from '@shared/validators/custom.validators';
import * as orderAction from '@store/actions/orders';
import * as fromRoot from '@store/reducers';

@Component({
  selector: 'app-new-order',
  templateUrl: './new-order.component.html',
  styleUrls: ['./new-order.component.scss']
})
export class NewOrderComponent implements OnInit {
  currentDate: Date = new Date();
  orderDate: Date = new Date();

  phoneNumberValid: boolean;

  orderNumberMonth: number;
  orderId: string;

  orderForm: FormGroup;

  constructor(
    public snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private store: Store<fromRoot.State>
  ) {}

  get isOrderFormInvalid(): boolean {
    return this.orderForm.invalid;
  }

  ngOnInit() {
    this.initForm();
    this.orderForm.valueChanges.subscribe(el => {
      this.orderForm.patchValue(
        { orderId: this.calculateOrderId(el.orderType, el.orderDate) },
        { emitEvent: false }
      );
    });
    this.store
      .select(store => store)
      .subscribe(state => {
        this.orderNumberMonth = state.orders.ids.length + 1;
      });
  }

  isFormValueInvalid(value: string): boolean {
    return this.orderForm.get(value).invalid;
  }

  onPhoneNumberError(error: boolean) {
    this.phoneNumberValid = !error;
  }

  onChangeDate(date: Date) {
    this.orderForm.patchValue({
      orderDate: moment(date, 'YYYY-MM-DD').format('YYYY-MM-DD')
    });
  }

  getPhoneNumber(phoneNumber: string) {
    this.orderForm.patchValue({
      phoneNumber
    });
  }

  save() {
    if (this.orderForm.valid) {
      this.orderForm.value.orderId = this.orderId;
      this.orderForm.value.date = moment(this.currentDate, 'YYYY-MM-DD').format(
        'YYYY-MM-DD'
      );
      this.store.dispatch(new orderAction.AddOne(this.orderForm.value));
      this.showSuccessSnackBar();
      this.orderForm.reset();
      this.orderDate = new Date();
    }
  }

  private showSuccessSnackBar() {
    this.snackBar.openFromComponent(SuccessSnackComponent, {
      duration: 1000
    });
  }

  private calculateOrderId(orderType: string, orderDate: Date): string {
    if (orderType && orderDate) {
      this.orderId = `${orderType.charAt(0)}-${moment(orderDate).format(
        'YY'
      )}${moment(orderDate).format('MM')}${this.orderNumberMonth}`;
    }
    return this.orderId;
  }

  private initForm() {
    this.orderForm = this.formBuilder.group({
      email: [null, [Validators.required, EmailValidator.isValid]],
      firstName: [null, Validators.required],
      lastName: [null, Validators.required],
      phoneNumber: [null, Validators.required],
      items: [null, Validators.required],
      orderType: [null, Validators.required],
      seller: [null, Validators.required],
      orderId: [{ value: null, disabled: true }, Validators.required],
      orderDate: [
        moment(this.orderDate, 'YYYY-MM-DD').format('YYYY-MM-DD'),
        Validators.required
      ],
      comment: [null]
    });
  }
}
