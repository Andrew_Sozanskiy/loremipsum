import { Routes } from '@angular/router';

import { StartComponent } from '@pages/start/start.component';

export const appRoutes: Routes = [
  { path: '', component: StartComponent },
  {
    path: 'orders',
    loadChildren: './pages/orders/orders.module#OrdersModule'
  },
  {
    path: 'new-order',
    loadChildren: './pages/new-order/new-order.module#NewOrderModule'
  }
];
