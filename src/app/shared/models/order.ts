interface IOrder {
  id: number;
  date: string;
  userEmail: string;
  userFirstName: string;
  userLastName: string;
  userTel: string;
  items: string;
  orderType: string;
  seller: string;
  orderId: string;
  orderDate: string;
  comment?: string;
}

export class Order implements IOrder {
  id: number;
  date: string;
  userEmail: string;
  userFirstName: string;
  userLastName: string;
  userTel: string;
  items: string;
  orderType: string;
  seller: string;
  orderId: string;
  orderDate: string;
  comment?: string;
}
