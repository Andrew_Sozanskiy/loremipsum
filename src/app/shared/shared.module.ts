import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ErrorSnackComponent } from '@shared/components/error-snack/error-snack.component';
import { HeaderComponent } from '@shared/components/header/header.component';
import { SuccessSnackComponent } from '@shared/components/success-snack/success-snack';

const COMPONENTS = [
  HeaderComponent,
  SuccessSnackComponent,
  ErrorSnackComponent
];

const MODULES = [RouterModule];

@NgModule({
  imports: [...MODULES],
  declarations: [...COMPONENTS],
  exports: [...COMPONENTS],
  entryComponents: [...COMPONENTS]
})
export class SharedModule {}
