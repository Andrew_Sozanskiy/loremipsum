import { Component } from '@angular/core';

@Component({
  selector: 'app-error-snack',
  templateUrl: './error-snack.component.html',
  styles: [
    `
      .error {
        color: red;
      }
    `
  ]
})
export class ErrorSnackComponent {}
