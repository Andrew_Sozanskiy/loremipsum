import { Component } from '@angular/core';

@Component({
  selector: 'app-success-snack',
  templateUrl: 'success-snack.html',
  styles: [
    `
      .success-order {
        color: green;
      }
    `
  ]
})
export class SuccessSnackComponent {}
