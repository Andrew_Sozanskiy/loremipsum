import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { AppComponent } from '@app/app.component';
import { appRoutes } from '@app/app.routes';
import { StoreModule } from '@ngrx/store';
import { StartComponent } from '@pages/start/start.component';
import { GridModule } from '@progress/kendo-angular-grid';
import { SharedModule } from '@shared/shared.module';
import { metaReducers, reducers } from '@store/reducers';

const COMPONENTS = [AppComponent, StartComponent];

const MODULES = [
  BrowserModule,
  BrowserAnimationsModule,
  CommonModule,
  RouterModule.forRoot(appRoutes),
  GridModule,
  StoreModule.forRoot(reducers, { metaReducers }),
  SharedModule
];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [...MODULES],
  entryComponents: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
